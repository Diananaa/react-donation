import { ILLCor1, ILLCor2, ILLCor3 } from '../../assets';
export default [
  {
    id: '1',
    title: 'Food Waste',
    description:
      'Food Waste adalah sisa makanan yang terbuang menjadi sampah makanan. Indonesia berada di posisi kedua setelah Arab Saudi dengan pelaku food waste terbanyak. \n\n Menurut Badan Ketahanan Pangan Kementerian Pertanian, food waste di Indonesia jika dikumpulkan dalam satu tahun jumlahnya mencapai 1.3 jutabton dengan rata rata 300 kg per tahunnya.',
    image: ILLCor1,
  },
  {
    id: '2',
    title: 'Makanan Kita',
    description:
      'Makanan kita merupakan sebuah platform yang berfungsi sebagai penyaluran food waste. Melalui aplikasi ini akan menghubungkan antara donatur dengan penerima donasi. \n\n Donasi dapat berupa makanan saji yang masih layak maupun makanan dalam bentuk raw material. Dengan "Makanan Kita", kita bisa membantu mengurangi permasalahan sampah makanan dan kekurangan makanan di Indonesia.  ',
    image: ILLCor2,
  },
  {
    id: '3',
    title: 'Kelebihan Makanan Kita',
    description:
      '1. Platform “Makanan Kita” berbasis regional sehingga mempermudah pendistribusian dari donatur ke penerima \n\n 2. Penyaluran raw material dan makanan saji yang masih layak mudah diakses oleh donatur maupun penerima. \n \n 3. Jalinan kemitraan dengan donatur maupun calon penerima donasi, sehingga platform ini lebih efektif dan efisien untuk dijalankan. \n\n 4. Tim khusus dari “Makanan Kita” juga akan turut membantu penyeleksian raw material yang akan didonasikan dan membantu pendistribusian. ',
    image: ILLCor3,
  },
];
