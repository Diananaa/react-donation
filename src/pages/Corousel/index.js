import React, { useRef, useState } from 'react';
import {
  Animated,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { IcDetail } from '../../assets';
import Paginator from '../../components/atoms/Button/Paginator';
import CorouselItem from '../../components/molecules/CorouselItem';
import { colors } from '../../utils';
import slides from './slides';

const Corousel = ({ navigation }) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const scrollx = useRef(new Animated.Value(0)).current;
  const slidesRef = useRef(null);

  const viewableItemsChange = useRef(({ viewableItems }) => {
    setCurrentIndex(viewableItems[0].index);
  }).current;

  const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50 }).current;

  return (
    <>
      <View style={styles.container}>
        <View style={styles.page}>
          <FlatList
            data={slides}
            renderItem={({ item }) => <CorouselItem item={item} />}
            horizontal
            showsHorizontalScrollIndicator={false}
            pagingEnabled
            bounces={false}
            keyExtractor={item => item.id}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: { contentOffset: { x: scrollx } },
                },
              ],
              {
                useNativeDriver: false,
              },
            )}
            scrollEventThrottle={32}
            onViewableItemsChanged={viewableItemsChange}
            viewabilityConfig={viewConfig}
          />
        </View>
        <Paginator data={slides} scrollx={scrollx} />
      </View>
      <View style={styles.conBtn}>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => navigation.replace('GetStarted')}>
          <IcDetail />
        </TouchableOpacity>
      </View>
    </>
  );
};
export default Corousel;

const styles = StyleSheet.create({
  conBtn: {
    width: '100%',
    height: 50,
    backgroundColor: colors.primary,
  },
  btn: {
    width: 40,
    height: 40,
    backgroundColor: colors.secondary,
    borderRadius: 999,
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    position: 'absolute',
    alignContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  page: { flex: 1 },
});
