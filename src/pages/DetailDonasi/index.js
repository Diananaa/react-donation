import CheckBox from '@react-native-community/checkbox';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Modal,
  Pressable,
} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { useDispatch, useSelector } from 'react-redux';
import { IcCalender } from '../../assets';
import { Button, Gap, Header, RowText, TextInput } from '../../components';
import {
  fetchCities,
  fetchDistricts,
  fetchProvinces,
  fetchVillages,
  getItem,
  storeDonation,
} from '../../redux/action';
import { colors, getData, useForm } from '../../utils';

const DetailDonasi = ({ navigation }) => {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [tglPickup, setTglPickup] = useState(moment().format('D MMMM YYYY'));
  const [pickUpDate, setPickupDate] = useState(moment().format('YYYY-MM-DD'));
  const [userProfile, setUserProfile] = useState({});
  const [alamat, setAlamat] = useState([]);
  const [tgl_donasi, setTglDonasi] = useState();
  const { provinces, cities, districts, villages } = useSelector(
    state => state.utilsReducer,
  );
  const { items } = useSelector(state => state.itemsReducer);
  const dispatch = useDispatch();
  const [alamatForm, setAlamatForm] = useForm({
    alamat: '',
    provinsi: '',
    kota: '',
    kecamatan: '',
    kelurahan: '',
    rw: '',
    rt: '',
    kode_pos: '',
  });

  useEffect(() => {
    navigation.addListener('focus', () => {
      setTglDonasi(moment().format('D MMMM YYYY'));
      setTglPickup(moment().format('D MMMM YYYY'));
      setPickupDate(moment().format('YYYY-MM-DD'));
      getData('userProfile').then(resMember => {
        setUserProfile(resMember);
        const street = {
          alamat: resMember.alamat,
          provinsi: resMember.provinsi,
          kota: resMember.kota,
          kecamatan: resMember.kecamatan,
          kelurahan: resMember.kelurahan,
          rw: resMember.rw,
          rt: resMember.rt,
          kode_pos: resMember.kode_pos,
        };
        setAlamat(street);
      });
      dispatch(getItem());
    });
  }, [navigation, dispatch]);

  const handleConfirm = date => {
    setTglPickup(moment(date).format('D MMMM YYYY'));
    setPickupDate(moment(date).format('YYYY-MM-DD'));
    hideDatePicker();
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const onCheck = value => {
    setToggleCheckBox(value);
    dispatch(fetchProvinces());
  };

  const getCities = provinsi_id => {
    if (provinsi_id !== 0) {
      setAlamatForm('provinsi', provinsi_id);
      dispatch(fetchCities(provinsi_id));
    }
  };

  const getDistricts = cities_id => {
    if (cities_id !== 0) {
      setAlamatForm('kota', cities_id);
      dispatch(fetchDistricts(cities_id));
    }
  };

  const getVillages = kecamatan_id => {
    if (kecamatan_id !== 0) {
      setAlamatForm('kecamatan', kecamatan_id);
      dispatch(fetchVillages(kecamatan_id));
    }
  };

  const yes = () => {
    setModalVisible(!modalVisible);
    const foodItems = JSON.stringify(items);
    const data = !toggleCheckBox
      ? { ...alamat, pick_up_date: pickUpDate, items: foodItems }
      : {
          ...alamatForm,
          pick_up_date: pickUpDate,
          items: foodItems,
        };
    dispatch(storeDonation(data, navigation));
  };
  const no = () => {
    setModalVisible(!modalVisible);
  };
  return (
    <View style={styles.page}>
      <Header single title="Detail Donasi" onBack={() => navigation.goBack()} />

      <Modal animationType="none" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              Apakah anda yakin untuk donasi?
            </Text>
            <View style={styles.btnStyle}>
              <View>
                <Pressable style={styles.buttonModal} onPress={yes}>
                  <Text style={styles.textStyle}>Ya</Text>
                </Pressable>
              </View>
              <View>
                <Gap width={14} />
              </View>
              <View>
                <Pressable style={styles.buttonModal} onPress={no}>
                  <Text style={styles.textStyle}>Tidak</Text>
                </Pressable>
              </View>
            </View>
          </View>
        </View>
      </Modal>

      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        <RowText title="Nama barang" />
        {items.length === 0 ? (
          <RowText deskripsi="Tidak ada barang" />
        ) : (
          <View>
            {items.map((item, index) => {
              return (
                <RowText
                  key={index}
                  deskripsi={item.nama_barang}
                  value={`${item.jumlah} ${item.quantity}`}
                />
              );
            })}
          </View>
        )}

        <RowText title="Tanggal" />
        <RowText deskripsi="Tanggal donasi" value={tgl_donasi} />
        <View>
          <View style={styles.containertgl}>
            <Text style={styles.deskripsi}>Tanggal pengambilan</Text>
            {tglPickup && (
              <View style={styles.tgl}>
                <Text style={styles.value('')}>{tglPickup}</Text>
              </View>
            )}
            <Gap width={4} />
            <TouchableOpacity style={styles.cal} onPress={showDatePicker}>
              <IcCalender />
            </TouchableOpacity>

            <DateTimePickerModal
              isVisible={isDatePickerVisible}
              mode="date"
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
            />
          </View>
        </View>

        <RowText title="Donatur" />
        <RowText deskripsi="Nama" value={userProfile.name} />
        <RowText deskripsi="No. hp" value={userProfile.no_telp} />
        <RowText deskripsi="Alamat" value={userProfile.alamat} />

        <View style={styles.pageBox}>
          <CheckBox
            disabled={false}
            value={toggleCheckBox}
            tintColors={colors.primary}
            onValueChange={newValue => onCheck(newValue)}
          />
          <View style={styles.conBox}>
            <Text style={styles.textBox}>
              Apakah alamat pengambilan berbeda?
            </Text>
          </View>
        </View>
        <View style={styles.input}>
          {toggleCheckBox && (
            <View>
              <Gap height={30} />
              <TextInput
                label="Alamat Lengkap"
                placeholder="Masukkan Alamat lengkap"
                value={alamatForm.alamat}
                onChangeText={value => setAlamatForm('alamat', value)}
              />
              <Gap height={12} />
              <TextInput
                label="Provinsi"
                placeholder="Pilih Provinsi"
                value={alamatForm.provinsi}
                onValueChange={value => getCities(value)}
                selectProvinsi
                selectItem={provinces}
              />
              <Gap height={12} />
              <TextInput
                label="Kota / Kabupaten"
                placeholder="Pilih Kota/kabupaten"
                value={alamatForm.kota}
                onValueChange={value => getDistricts(value)}
                selectProvinsi
                selectItem={cities}
              />
              <Gap height={12} />
              <TextInput
                label="Kecamatan"
                placeholder="Pilih Kecamatan"
                value={alamatForm.kecamatan}
                onValueChange={value => getVillages(value)}
                selectProvinsi
                selectItem={districts}
              />
              <Gap height={12} />
              <TextInput
                label="Kelurahan"
                placeholder="Pilih Kelurahan"
                value={alamatForm.kelurahan}
                onValueChange={value => setAlamatForm('kelurahan', value)}
                selectProvinsi
                selectItem={villages}
              />
              <Gap height={12} />
              <View style={styles.alamat}>
                <View style={styles.form}>
                  <TextInput
                    label="RW"
                    placeholder="Masukkan RW"
                    value={alamatForm.rw}
                    style={styles.center}
                    keyboardType="numeric"
                    onChangeText={value => setAlamatForm('rw', value)}
                  />
                </View>
                <Gap height={12} width={32} />
                <View style={styles.form}>
                  <TextInput
                    label="RT"
                    placeholder="Masukkan RT"
                    value={alamatForm.rt}
                    style={styles.center}
                    keyboardType="numeric"
                    onChangeText={value => setAlamatForm('rt', value)}
                  />
                </View>
              </View>
              <Gap height={12} />
              <TextInput
                label="Kode Pos"
                placeholder="Masukkan kode pos"
                value={alamatForm.kode_pos}
                style={styles.center}
                keyboardType="numeric"
                onChangeText={value => setAlamatForm('kode_pos', value)}
              />
              <Gap height={60} />
              {alamatForm.alamat !== '' &&
              alamatForm.provinsi !== '' &&
              alamatForm.kota !== '' &&
              alamatForm.kecamatan !== '' &&
              alamatForm.kelurahan !== '' &&
              alamatForm.rw !== '' &&
              alamatForm.rt !== '' &&
              alamatForm.kode_pos !== '' ? (
                <View style={styles.button}>
                  <Button
                    tittle="Donasi"
                    onPress={() => setModalVisible(true)}
                  />
                </View>
              ) : (
                <Button tittle="Donasi" type="disable" disable />
              )}
            </View>
          )}
          <View>
            <Gap height={60} />
            {!toggleCheckBox && (
              <Button tittle="Donasi" onPress={() => setModalVisible(true)} />
            )}
          </View>
        </View>
        <Gap height={50} />
      </ScrollView>
    </View>
  );
};
export default DetailDonasi;

const styles = StyleSheet.create({
  buttonModal: {
    borderRadius: 10,
    paddingVertical: 12,
    elevation: 2,
    backgroundColor: colors.primary,
    width: 117,
    height: 42,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(52,64,125,0.5)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    borderColor: colors.primary,
    borderWidth: 1,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontFamily: 'Poppins-Light',
    color: colors.primary,
    fontSize: 16,
  },
  textStyle: {
    color: colors.secondary,
    fontFamily: 'Poppins-Light',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btnStyle: {
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  center: {
    textAlign: 'center',
    borderWidth: 1,
    paddingHorizontal: 30,
    borderRadius: 8,
    paddingVertical: 12,
    borderColor: colors.primary,
    color: colors.primary,
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
  },
  tgl: {
    borderColor: colors.primary,
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 4,
    paddingHorizontal: 8,
  },
  containertgl: {
    flexDirection: 'row',
    paddingTop: 0,
    paddingBottom: 15,
    margin: 0,
  },
  deskripsi: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: colors.primary,
    flex: 1,
  },
  value: placeholder => ({
    fontSize: 14,
    fontFamily: 'Poppins-Light',
    color: placeholder ? colors.fourth : colors.primary,
  }),
  button: {
    height: 50,
  },
  note: {
    fontFamily: 'Poppins-Light',
    color: colors.primary,
    fontSize: 8,
  },
  conBox: {
    justifyContent: 'center',
    flex: 1,
  },
  input: {
    marginTop: -15,
  },
  textBox: {
    fontFamily: 'Poppins-Light',
    color: colors.primary,
    fontSize: 11,
  },
  pageBox: {
    flexDirection: 'row',
  },
  container: {
    paddingHorizontal: 24,
  },
  page: {
    backgroundColor: colors.tertiary,
    flex: 1,
  },
  alamat: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  form: { flex: 1 },
});
