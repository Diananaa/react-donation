import Biodata from './Biodata';
import Corousel from './Corousel';
import GetStarted from './GetStarted';
import Login from './Login';
import Register from './Register';
import SplashScreen from './SplashScreen';
import History from './History';
import Home from './Home';
import Profile from './Profile';
import AddDonasi from './AddDonasi';
import DetailDonasi from './DetailDonasi';
import SplashDone from './SplashDone';
import UpdateBiodata from './UpdateBiodata';
import UpdatePassword from './UpdatePassword';
import DetailHistory from './DetailHistory';

export {
  UpdatePassword,
  UpdateBiodata,
  SplashDone,
  AddDonasi,
  DetailDonasi,
  Biodata,
  Corousel,
  GetStarted,
  Login,
  Register,
  SplashScreen,
  History,
  Home,
  Profile,
  DetailHistory,
};
