import Axios from 'axios';
import { API_HOST } from '../../config';
import { showMessage } from '../../utils';
import { setLoading } from './global';

export const fetchTerms = () => dispatch => {
  dispatch(setLoading(true));
  Axios.get(`${API_HOST.url}/terms`)
    .then(res => {
      dispatch(setLoading(false));
      dispatch({ type: 'SET_TERMS', value: res.data.data });
    })
    .catch(err => {
      showMessage(err?.response?.data?.data?.message);
    });
};
