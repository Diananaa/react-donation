const initTermState = {
  terms: {},
};

export const termsReducer = (state = initTermState, action) => {
  if (action.type === 'SET_TERMS') {
    return {
      ...state,
      terms: action.value,
    };
  }

  return state;
};
