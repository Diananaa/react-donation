const initQuantitytate = {
  quantity: [],
};

export const quantityReducer = (state = initQuantitytate, action) => {
  if (action.type === 'SET_QUANTITY') {
    return {
      ...state,
      quantity: action.value,
    };
  }

  return state;
};
