import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Modal,
  Pressable,
} from 'react-native';
import { IcDelete } from '../../../assets';
import { API_HOST } from '../../../config';
import { colors } from '../../../utils';
import { Gap } from '../../../components';

const Card = ({ item, onPress }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const yes = () => {
    setModalVisible(!modalVisible);
    onPress();
  };
  const no = () => {
    setModalVisible(!modalVisible);
  };
  return (
    <>
      <Modal animationType="none" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              {`Apakah anda yakin mau mengapus ${item.nama_barang} dari donasi?`}
            </Text>
            <View style={styles.btnStyle}>
              <View>
                <Pressable style={styles.buttonModal} onPress={yes}>
                  <Text style={styles.textStyle}>Ya</Text>
                </Pressable>
              </View>
              <View>
                <Gap width={14} />
              </View>
              <View>
                <Pressable style={styles.buttonModal} onPress={no}>
                  <Text style={styles.textStyle}>Tidak</Text>
                </Pressable>
              </View>
            </View>
          </View>
        </View>
      </Modal>

      <View activeOpacity={0.7} style={styles.container}>
        <TouchableOpacity
          style={styles.delete}
          onPress={() => setModalVisible(true)}>
          <IcDelete />
        </TouchableOpacity>
        <View style={styles.card}>
          <View>
            <Image
              source={{ uri: `${API_HOST.storage}food/${item.foto}` }}
              style={styles.image}
            />
          </View>
          <View style={styles.text}>
            <Text style={styles.title}>{item.nama_barang}</Text>
            <Text
              style={styles.porsi}>{`${item.jumlah} ${item.quantity}`}</Text>
          </View>
        </View>
      </View>
    </>
  );
};
export default Card;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(52,64,125,0.5)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    borderColor: colors.primary,
    borderWidth: 1,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontFamily: 'Poppins-Light',
    color: colors.primary,
    fontSize: 16,
  },
  btnStyle: {
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  buttonModal: {
    borderRadius: 10,
    paddingVertical: 12,
    elevation: 2,
    backgroundColor: colors.primary,
    width: 117,
    height: 42,
  },
  textStyle: {
    color: colors.secondary,
    fontFamily: 'Poppins-Light',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  title: {
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
  },
  porsi: {
    fontSize: 13,
    fontFamily: 'Poppins-Regular',
  },
  text: {
    marginLeft: 20,
    marginRight: 30,
    justifyContent: 'center',
    flex: 1,
  },
  delete: {
    padding: 7,
    position: 'absolute',
    top: 13,
    right: 13,
  },
  card: {
    flexDirection: 'row',
  },
  container: {
    paddingVertical: 5,
    paddingLeft: 5,
    borderWidth: 1,
    borderColor: colors.primary,
    borderRadius: 8,
    marginTop: 10,
  },
  image: {
    height: 90,
    width: 130,
    borderRadius: 8,
  },
});
