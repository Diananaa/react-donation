import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { IcDetail } from '../../../assets';
import { colors } from '../../../utils';
import { Gap } from '../../../components';

const CardProses = ({ title, tgl, banyak, status, onPress }) => {
  return (
    <View style={styles.page}>
      <View>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.container}>
          {banyak > 0 ? (
            <View style={styles.banyak}>
              <Text style={styles.desc}>+ {banyak}</Text>
              <Gap width={4} />
              <Text style={styles.desc}>barang</Text>
            </View>
          ) : (
            <View style={styles.banyak}>
              <Text style={styles.desc}>1</Text>
              <Gap width={4} />
              <Text style={styles.desc}>barang</Text>
            </View>
          )}
          <View style={styles.containerDot}>
            <Text style={styles.dot}>.</Text>
          </View>
          <Text style={styles.desc}>{tgl}</Text>
        </View>
      </View>
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.detail}
        onPress={onPress}>
        <View style={styles.status(status)} />
        <IcDetail />
      </TouchableOpacity>
    </View>
  );
};
export default CardProses;

const styles = StyleSheet.create({
  banyak: {
    flexDirection: 'row',
  },
  containerDot: {
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: 18,
  },
  title: {
    fontSize: 16,
    fontFamily: 'Poppins-Regular',
    color: colors.primary,
  },
  desc: {
    fontSize: 13,
    fontFamily: 'Poppins-Regular',
    color: colors.primary,
  },
  dot: {
    fontFamily: 'Poppins-Regular',
    color: colors.primary,
  },
  detail: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
    width: '20%',
    maxWidth: 100,
  },
  page: {
    flexDirection: 'row',
    paddingVertical: 8,
    paddingHorizontal: 12,
    justifyContent: 'space-between',
    borderWidth: 1,
    borderRadius: 8,
    borderColor: colors.primary,
    marginBottom: 12,
  },
  container: {
    flexDirection: 'row',
  },
  status: status => ({
    width: '70%',
    height: 12,
    borderRadius: 50,
    backgroundColor: status,
  }),
});
