import React from 'react';
import { StyleSheet, View, useWindowDimensions, Animated } from 'react-native';
import { colors } from '../../../utils';

const Paginator = ({ data, scrollx }) => {
  const { width } = useWindowDimensions();
  return (
    <View style={styles.page}>
      {data.map((_, i) => {
        const inputRange = [(i - 1) * width, 1 * width, (i + 1) * width];
        const dotwidth = scrollx.interpolate({
          inputRange,
          outputRange: [10, 20, 10],
          extrapolate: 'clamp',
        });
        const opacity = scrollx.interpolate({
          inputRange,
          outputRange: [0.3, 1, 0.3],
          extrapolate: 'clamp',
        });
        return (
          <Animated.View
            style={[styles.dot, { width: dotwidth, opacity }]}
            key={i.toString()}
          />
        );
      })}
    </View>
  );
};
export default Paginator;

const styles = StyleSheet.create({
  dot: {
    height: 10,
    borderRadius: 5,
    backgroundColor: colors.secondary,
    marginHorizontal: 8,
  },
  page: {
    flexDirection: 'row',
    height: 64,
  },
});
