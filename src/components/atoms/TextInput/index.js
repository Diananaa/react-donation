import React from 'react';
import { StyleSheet, Text, View, TextInput as TextInputRN } from 'react-native';
import { Gap } from '../../atoms';
import { colors } from '../../../utils';
import { Picker } from '@react-native-picker/picker';

const TextInput = ({
  label,
  placeholder,
  select,
  selectProvinsi,
  value,
  onValueChange,
  selectItem,
  secureTextEntry,
  onChangeText,
  editable,
  ...restProps
}) => {
  if (selectProvinsi) {
    return (
      <View>
        <Text style={styles.label}>{label}</Text>
        <Gap height={14} />
        <View style={styles.picker}>
          <Picker
            selectedValue={value}
            onValueChange={onValueChange}
            dropdownIconColor="grey">
            <Picker.item
              key=""
              style={styles.pickerItem}
              label={placeholder}
              value=""
            />
            {selectItem.map(item => {
              return (
                <Picker.Item
                  key={item.id}
                  style={styles.pickerItem}
                  label={item.name}
                  value={item.id}
                  {...restProps}
                />
              );
            })}
          </Picker>
        </View>
      </View>
    );
  }

  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <Gap height={14} />
      <View style={styles.inputForm(editable)}>
        <TextInputRN
          style={styles.input}
          placeholder={placeholder}
          placeholderTextColor={colors.fourth}
          value={value}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          editable={!editable}
          {...restProps}
        />
      </View>
    </View>
  );
};
export default TextInput;

const styles = StyleSheet.create({
  pickerItem: {
    backgroundColor: colors.tertiary,
    color: colors.primary,
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
  },
  picker: {
    borderWidth: 1,
    borderColor: colors.primary,
    borderRadius: 10,
    paddingHorizontal: 4,
    flex: 1,
  },
  label: {
    fontSize: 16,
    fontFamily: 'Poppins-Regular',
    color: colors.primary,
  },
  input: {
    borderWidth: 1,
    paddingHorizontal: 30,
    borderRadius: 8,
    paddingVertical: 12,
    borderColor: colors.primary,
    color: colors.primary,
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
  },
  inputForm: editable => ({
    backgroundColor: editable ? '#F2F2F2' : colors.tertiary,
  }),
});
