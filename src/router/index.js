import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { BottomNavigator } from '../components';
import {
  AddDonasi,
  Biodata,
  Corousel,
  DetailDonasi,
  DetailHistory,
  GetStarted,
  History,
  Home,
  Login,
  Profile,
  Register,
  SplashDone,
  SplashScreen,
  UpdateBiodata,
  UpdatePassword,
  DetailTransaksi,
  DetailProfile,
} from '../pages';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="History" component={History} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
};

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="SplashScreen">
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DetailTransaksi"
        component={DetailTransaksi}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Corousel"
        component={Corousel}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="GetStarted"
        component={GetStarted}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Biodata"
        component={Biodata}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="AddDonasi"
        component={AddDonasi}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DetailDonasi"
        component={DetailDonasi}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DetailProfile"
        component={DetailProfile}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SplashDone"
        component={SplashDone}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="UpdateBiodata"
        component={UpdateBiodata}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="UpdatePassword"
        component={UpdatePassword}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DetailHistory"
        component={DetailHistory}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export default Router;
